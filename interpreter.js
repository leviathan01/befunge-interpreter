const X_MAX = 80, Y_MAX = 25;

const directions = {
    LEFT: "left",
    RIGHT: "right",
    UP: "up",
    DOWN: "down",
}

// Stores ints
var stack = [];

var direction = directions.RIGHT;
var posX = 0;
var posY = 0;

// If the program is done running.
var done = false;

// If all symbols should be pushed to the stack.
var litural = false;

// Setup the program space
var program = new Array(X_MAX);
function makeWorld() {
    program = new Array(X_MAX);
    for (var i = 0; i < X_MAX; i++) {
        program[i] = new Array(Y_MAX);
        for (var j = 0; j < Y_MAX; j++) {
            program[i][j] = " ";
        }
    }
}

makeWorld();

program[0][0] = "1";
program[1][0] = "3";
program[2][0] = "+";
program[3][0] = ",";
program[4][0] = "@";


// All of the operations that we support.
var operations = new Map();

// Change direction
operations.set(">", () => {
    direction = directions.RIGHT;
});
operations.set("<", () => {
    direction = directions.LEFT;
});
operations.set("^", () => {
    direction = directions.UP;
});
operations.set("v", () => {
    direction = directions.DOWN;
});
operations.set("?", () => {
    var r =  Math.floor(Math.random() * 4);     // returns a random integer from 0 to 3
    switch (r) {
        case 0: direction = directions.UP; break;
        case 1: direction = directions.DOWN; break;
        case 2: direction = directions.LEFT; break;
        case 3: direction = directions.RIGHT; break;
    }
})

// End the program
operations.set("@", () => {
    done = true;
    console.log("Done!");
});

// Math
operations.set("+", () => {
    var a = pop();
    var b = pop();

    stack.push((+a) + (+b));
});
operations.set("-", () => {
    var a = pop();
    var b = pop();

    stack.push((+b) - (+a));
});
operations.set("*", () => {
    var a = pop();
    var b = pop();

    stack.push((+a) * (+b));
});
operations.set("/", () => {
    var a = pop();
    var b = pop();

    if (a === 0) {
        alert("divide by zero");
        done = true;
        return;
    }

    stack.push(Math.floor(b/a));
});
operations.set("%", () => {
    var a = pop();
    var b = pop();

    if (a === 0) {
        alert("mod by zero");
        done = true;
        return;
    }

    stack.push(b % a);
});

// IO
operations.set(",", () => {
    output(String.fromCharCode(+pop()));
});
operations.set(".", () => {
    var value = pop();

    output(value + " ");
});
operations.set("g", () => {
    var x = pop();
    var y = pop();
    stack.push(program[x][y].charCodeAt(0));
});
operations.set("p", () => {
    var x = pop();
    var y = pop();
    var v = pop();
    console.log(v);
    program[x][y] = String.fromCharCode(v);
});

operations.set('"', () => {
    litural = !litural
});

operations.set("#", () => {
    // Skip next cell
    move();
});

// Stack ops
operations.set(":", () => {
    var tmp = pop();
    stack.push(tmp);
    stack.push(tmp);
});
operations.set("\\", () => {
    var a = pop();
    var b = pop();
    stack.push(a);
    stack.push(b);
});
operations.set("$", pop);

// COnditions
operations.set("_", () => {
    var tmp = pop();
    if (tmp === "0" || tmp === 0) {
        direction = directions.RIGHT;
    } else {
        direction = directions.LEFT;
    }
});
operations.set("|", () => {
    var tmp = pop();
    if (tmp === 0) {
        direction = directions.DOWN;
    } else {
        direction = directions.UP;
    }
});
operations.set("!", () => {
    if (pop() === 0) {
        stack.push(1);
    } else {
        stack.push(0);
    }
});
operations.set("'", () => {
    var a = pop();
    var b = pop();

    stack.push(b>a ? 1 : 0);
});

function step() {
    // Check if the program is done.
    if (done) {
        return;
    }

    updateDisplay();

    // Get the current symbol
    var currentSymbol = program[posX][posY];

    
    // Process the current symbol
    // Check if the symbol is a number or if we are in litural mode.
    if (litural && currentSymbol !== '"') {
        stack.push(currentSymbol.charCodeAt(0));
    } else if ((!isNaN(currentSymbol) && currentSymbol !== " ")) {
        stack.push(currentSymbol);
    } else if (operations.has(currentSymbol)) {
        operations.get(currentSymbol)();
    } else if (currentSymbol === " " || !currentSymbol) {
        // noop
    } else {
        alert(`invalid symbol '${currentSymbol}' at (${posX}, ${posY})`);
        done = true;
    }

    // Move
    move();
}

function move() {
    switch (direction) {
        case directions.LEFT: posX = mod((posX - 1), X_MAX); break;
        case directions.RIGHT: posX = mod((posX + 1), X_MAX); break;
        case directions.UP: posY = mod((posY - 1), Y_MAX); break;
        case directions.DOWN: posY = mod((posY + 1), Y_MAX); break;
    }
}

// Mod with negitive value roll over
function mod(n, m) {
    return ((n % m) + m) % m;
}

function updateDisplay() {
    var programStr = "";
    for (var row = 0; row < Y_MAX; row++) {
        for (var column = 0; column < X_MAX; column++) {
            var currentSymbol = program[column][row] ? program[column][row] : " ";
            if (row === posY && column === posX) {
                programStr += `<span style="background-color:coral;">${escapeChar(currentSymbol)}</span>`;
            } else {
                programStr += escapeChar(currentSymbol);
            }
        }
        programStr += "<br />";
    }
    document.getElementById("program").innerHTML = programStr;
}

function escapeChar(str) {
    switch(str) {
        default: return str;
    }
}

// Start a new program
function start() {
    step();
    setInterval(step, 100);
}

function restart() {
    posX = 0;
    posY = 0;

    done = false;
    litural = false;

    stack = [];

    direction = directions.RIGHT;

    document.getElementById("output").innerHTML = "";
}

// Load the code in the text input.
function loadCode() {
    var input = document.getElementById("code").value;
    makeWorld();
    for (var row = 0; row < Y_MAX; row++) {
        for (var column = 0; column < X_MAX; column++) {
            if (input.slice(0, 1) === '\n') {
                input = input.slice(1);
                break;
            }

            program[column][row] = input.slice(0, 1);
            if (input.length === 1) {
                restart();
                return;
            }
            input = input.slice(1);
        }
    }
    restart();
}

function output(str) {
    document.getElementById("output").innerHTML += str;
}

function pop() {
    if (stack.length < 1) {
        return 0;
    } else {
        return stack.pop();
    }
}